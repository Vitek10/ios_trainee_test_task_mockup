//
//  ConverterViewController.swift
//  iOS_trainee_test_task_mockup
//
//  Created by Разработчик on 15.12.2020.
//

import UIKit

class ConverterViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var valueFirstTextField: UITextField!
    @IBOutlet weak var valueSecondTextField: UITextField!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var nameCurrencyLabel: UILabel!
    
    let parser = Parser()
    var currencies = [Currencie]()
    var activeCurrency = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        parser.parse{
            data in
            self.currencies = data.filter{ $0.cc == "USD" || $0.cc == "EUR"}
            self.currencies.append(Currencie(rate: 1, cc: "UAH"))
            DispatchQueue.main.async {
                self.pickerView.reloadAllComponents()
            }
            
        }
        pickerView.delegate = self
        pickerView.dataSource = self
        valueFirstTextField.addTarget(self, action: #selector(updateViewFirst), for: .editingChanged)
        valueSecondTextField.addTarget(self, action: #selector(updateViewSecond), for: .editingChanged)
    }
    
    @objc func updateViewFirst(input: Double) {
        guard let amountText = valueFirstTextField.text, let theAmountText = Double(amountText) else {return}
        if valueFirstTextField.text != ""{
            let total = theAmountText * activeCurrency
            valueSecondTextField.text = String(format: "%.2f", total)
        }
    }
    @objc func updateViewSecond(input: Double) {
        guard let amountText = valueSecondTextField.text, let theAmountText = Double(amountText) else {return}
        if valueSecondTextField.text != " "{
            let total = theAmountText / activeCurrency
            valueFirstTextField.text = String(format: "%.2f", total)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return currencies.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return currencies[row].cc
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        nameCurrencyLabel.text = currencies[row].cc
        activeCurrency = currencies[row].rate
        updateViewFirst(input: activeCurrency)
        updateViewSecond(input: activeCurrency)
    }

}
