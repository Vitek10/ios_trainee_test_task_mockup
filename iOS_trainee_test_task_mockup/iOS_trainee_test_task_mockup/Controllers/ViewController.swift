//
//  ViewController.swift
//  iOS_trainee_test_task_mockup
//
//  Created by Разработчик on 15.12.2020.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    let parser = Parser()
    var currencies = [Currencie]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        parser.parse{
            data in
            self.currencies = data.filter{ $0.cc == "USD" || $0.cc == "EUR"}
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }

}

extension ViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ViewControllerTableViewCell
        let currency: Currencie
        currency = currencies[indexPath.row]
        cell.labelCurrency.text = currency.cc
        cell.labelRate.text = String(round(10*currency.rate)/10)
        return cell
    }
    
    
}

