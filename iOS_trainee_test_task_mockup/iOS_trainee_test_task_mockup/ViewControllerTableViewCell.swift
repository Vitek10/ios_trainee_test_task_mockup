//
//  ViewControllerTableViewCell.swift
//  iOS_trainee_test_task_mockup
//
//  Created by Разработчик on 15.12.2020.
//

import UIKit

class ViewControllerTableViewCell: UITableViewCell {
    @IBOutlet weak var labelCurrency: UILabel!
    @IBOutlet weak var labelRate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
