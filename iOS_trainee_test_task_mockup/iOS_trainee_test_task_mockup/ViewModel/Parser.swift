import Foundation


struct Parser {
    
    func parse(comp: @escaping ([Currencie])->()){
        let api = URL(string: "http://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json")
        URLSession.shared.dataTask(with: api!){
            data, response, error in
            if error != nil{
                //print(error?.localizedDescription)
                return
            }
            do{
                let result = try JSONDecoder().decode([Currencie].self, from: data!)
                comp(result)
            }
            catch{
                
            }
        }.resume()
    }
}
